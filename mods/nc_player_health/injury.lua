-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local injured = modname .. ":injured"
nodecore.register_virtual_item(injured, {
		description = "Injury",
		inventory_image = modname .. "_injured.png"
	})

nodecore.register_healthfx({
		item = injured,
		getqty = function(player)
			return 1 - nodecore.getphealth(player) / 8
		end
	})
