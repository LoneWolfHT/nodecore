-- LUALOCALS < ---------------------------------------------------------
local include, nodecore
    = include, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.amcoremod()

include("adze")
include("plank")
include("staff")
include("tools")
include("ladder")
include("shelf")
include("rake")
