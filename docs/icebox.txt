========================================================================
ICEBOX: Low-Priority Ideas
------------------------------------------------------------------------

  ####   #####    ####      #    #       ######  #####    ####
 #       #    #  #    #     #    #       #       #    #  #
  ####   #    #  #    #     #    #       #####   #    #   ####
      #  #####   #    #     #    #       #       #####        #
 #    #  #       #    #     #    #       #       #   #   #    #
  ####   #        ####      #    ######  ######  #    #   ####

- Consider quantizing/palletizing textures.

- Tree soil rate should bias closer nodes to have higher impact...?

- Should tree growth rate start out faster but slow down?
	- This could make the decision to harvest trees early vs
	  wait for them to mature more nuanced.

- Add hint for recycling glass(es) to sand?

- Consider checking for a nearby active player before running
  "fast-evolving" ABMs that don't normally work offline, like fire
  extinguishing/spreading?
	- This would make gameplay more fair for those who tend to
	  go AFK suddenly and can't return for a long time.

- API for recursing through embedded inventories, e.g. in totes.
	- Lux reaction cannot sense other lux inside totes.

- Player APIs:
	- Physics/acceleration, for waterflow, conveyors, etc.

- When can we remove the issue7020 workaround?

- Lens strobing rate limiting of some kind?
	- Require cool-downs after a lot of toggling?
	- Add histerisis or switch-on/charge-up delay?
	- Could this be an accessibility issue?

- "Smart" optics placement?
	- Pick initial rotation automatically to have the most utility
	  according to some heuristics.
	- May involve expanding or redesigning the optics API
	  significantly...?

- Make separate walkable/non-walkable stack nodes.
	- Should sticks and eggcorns be non-walkable?

- Code Quality.
	- Scripts to validate dependency graph.
		- Tag deps directly in code at place of use.
		- Auto-generate mod.conf / depends.txt
	- Scripts for automatic metapackage mods?
	- Git hooks (and git hook setup scripts)?

- API Cleanup
	- Further nc_api break-up, clean up util functions.
	- Heat API
		- Quenched, Flames as numbers or nil?
	- Unify nc_items and visinv API.
		- Stack nodes are "special", get first-class support.

- Make neighbor/distance calcs consistent.
	- We're using scan_flood in some places, face checks in others,
	  and find_nodes_in_area (cuboid) in others.
	- Diamond shape would be most consistent.
	- Should require face touching, not diagonal.

- Social features
	- Randomize player appearance/colors.
		- Shirt/pants, possibly w/ stripes/patterns
		- Skin color, hair color, eye color?
	- Support 64x64 dual-layer MC skins?
	- Add a cloak/hood or other clothing/acc surfaces?

........................................................................
========================================================================
